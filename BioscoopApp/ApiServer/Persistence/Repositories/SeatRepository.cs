﻿using ApiServer.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Persistence.Repositories
{
    public interface ISeatRepository
    {
        Task<bool> IsReserved(IEnumerable<int> ids, int showId);
        Task Add(IEnumerable<Seat> values);
        Task Remove(IEnumerable<Seat> values);
        Task<List<Seat>> List(IEnumerable<int> ids);
    }
    
    public class SeatRepository : ISeatRepository
    {
        private readonly DataContext _context;

        public SeatRepository(DataContext context)
        {
            _context = context;
        }

        public Task<bool> IsReserved(IEnumerable<int> ids, int showId)
        {
            return _context.Seats
                .Include(x => x.Reservations.Where(x => x.ShowId == showId))
                .AnyAsync(x => x.Reservations.Any() && ids.Contains(x.Id));
        }

        public Task Add(IEnumerable<Seat> values)
        {
            _context.Seats.AddRange(values);
            return _context.SaveChangesAsync();
        }

        public Task Remove(IEnumerable<Seat> values)
        {
            _context.Seats.RemoveRange(values);
            return _context.SaveChangesAsync();
        }

        public Task<List<Seat>> List(IEnumerable<int> ids)
        {
            return _context.Seats.Where(x => ids.Contains(x.Id)).ToListAsync();
        }
    }
}