﻿using ApiServer.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Persistence.Repositories
{
    public interface IMovieRepository
    {
        Task<Movie> Get(int id);
        Task Add(Movie value);
        Task Update(Movie value);
        Task<List<Movie>> List(bool currentOnly = false, bool activeOnly = true);
    }
    
    public class MovieRepository : IMovieRepository
    {
        private readonly DataContext _context;

        public MovieRepository(DataContext context)
        {
            _context = context;
        }

        public Task<Movie> Get(int id)
        {
            return _context.Movies
                .Include(x => x.Classifications)
                .Include(x => x.Shows.Where(x => x.Active && x.StartTime > DateTime.Now))
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task Add(Movie value)
        {
            _context.Movies.Add(value);
            return _context.SaveChangesAsync();
        }

        public Task Update(Movie value)
        {
            _context.Movies.Update(value);
            return _context.SaveChangesAsync();
        }

        public Task<List<Movie>> List(bool currentOnly = false, bool activeOnly = true)
        {
            var query = _context.Movies
                .Include(x => x.Classifications)
                .AsQueryable();
            if (currentOnly) query = query.Where(x => x.Shows.Any(x => x.Active && x.StartTime > DateTime.Now));
            if (activeOnly) query = query.Where(x => x.Active);
            return query.ToListAsync();
        }
    }
}