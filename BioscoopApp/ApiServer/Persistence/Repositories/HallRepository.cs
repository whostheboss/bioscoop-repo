﻿using ApiServer.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Persistence.Repositories
{
    public interface IHallRepository
    {
        Task<Hall> Get(int id);
        Task Add(Hall value);
        Task Update(Hall value);
        Task<List<Hall>> List(bool activeOnly = true);
    }
    
    public class HallRepository : IHallRepository
    {
        private readonly DataContext _context;

        public HallRepository(DataContext context)
        {
            _context = context;
        }

        public Task<Hall> Get(int id)
        {
            return _context.Halls
                .Include(x => x.Seats)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task Add(Hall value)
        {
            _context.Halls.Add(value);
            return _context.SaveChangesAsync();
        }

        public Task Update(Hall value)
        {
            _context.Halls.Update(value);
            return _context.SaveChangesAsync();
        }

        public Task<List<Hall>> List(bool activeOnly = true)
        {
            var query = _context.Halls
                .Include(x => x.Seats)
                .AsQueryable();
            if (activeOnly) query = query.Where(x => x.Active);
            return query.ToListAsync();
        }
    }
}