﻿using ApiServer.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Persistence.Repositories
{
    public interface IShowRepository
    {
        Task<Show> Get(int id);
        Task Add(Show value);
        Task Update(Show value);
        Task<List<Show>> List(bool activeOnly = true);
    }
    
    public class ShowRepository : IShowRepository
    {
        private readonly DataContext _context;

        public ShowRepository(DataContext context)
        {
            _context = context;
        }

        public Task<Show> Get(int id)
        {
            return _context.Shows
                .Include(x => x.Movie)
                .Include(x => x.Hall)
                .ThenInclude(x => x.Seats)
                .ThenInclude(x => x.Reservations)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task Add(Show value)
        {
            _context.Shows.Add(value);
            return _context.SaveChangesAsync();
        }

        public Task Update(Show value)
        {
            _context.Shows.Update(value);
            return _context.SaveChangesAsync();
        }

        public Task<List<Show>> List(bool activeOnly = true)
        {
            var query = _context.Shows
                .Include(x => x.Hall)
                .Include(x => x.Movie)
                .Where(x => x.StartTime > DateTime.Now);
            if (activeOnly) query = query.Where(x => x.Active);
            return query.ToListAsync();
        }
    }
}