﻿using ApiServer.Models.Entities;
using System.Threading.Tasks;

namespace ApiServer.Persistence.Repositories
{
    public interface IReservationRepository
    {
        Task Add(Reservation value);
    }
    
    public class ReservationRepository : IReservationRepository
    {
        private readonly DataContext _context;

        public ReservationRepository(DataContext context)
        {
            _context = context;
        }

        public Task Add(Reservation value)
        {
            _context.Reservations.Add(value);
            return _context.SaveChangesAsync();
        }
    }
}