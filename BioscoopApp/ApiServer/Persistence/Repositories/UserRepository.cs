﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using User = ApiServer.Models.Entities.User;

namespace ApiServer.Persistence.Repositories
{
    public interface IUserRepository
    {
        Task<User> Get(string email);
        Task Add(User value);
        Task Update(User value);
    }
    
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }

        public Task<User> Get(string email)
        {
            return _context.Users.FirstOrDefaultAsync(x => x.Email == email);
        }

        public Task Add(User value)
        {
            _context.Users.Add(value);
            return _context.SaveChangesAsync();
        }

        public Task Update(User value)
        {
            _context.Users.Update(value);
            return _context.SaveChangesAsync();
        }
    }
}