﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ApiServer.Persistence.Migrations
{
    public partial class ReservationCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Code",
                table: "Reservations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_Code",
                table: "Reservations",
                column: "Code",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Reservations_Code",
                table: "Reservations");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Reservations");
        }
    }
}
