﻿using ApiServer.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApiServer.Persistence
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Classification> Classifications { get; set; }
        public DbSet<Hall> Halls { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<Show> Shows { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(x => x.Email).IsUnique();
            modelBuilder.Entity<Reservation>().HasIndex(x => x.Code).IsUnique();
            modelBuilder.Entity<Seat>().HasIndex(x => new {x.HallId, x.Row, x.Number}).IsUnique();
        }
    }
}