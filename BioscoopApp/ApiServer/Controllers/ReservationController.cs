using ApiServer.Models.Entities;
using ApiServer.Payloads.Requests;
using ApiServer.Persistence.Repositories;
using ApiServer.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ApiServer.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReservationsController : ControllerBase
    {
        private readonly IReservationRepository _reservationRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISeatRepository _seatRepository;
        private readonly IMailService _mailService;
        private readonly IMapper _mapper;

        public ReservationsController(IReservationRepository reservationRepository, IUserRepository userRepository, ISeatRepository seatRepository, IMailService mailService, IMapper mapper)
        {
            _reservationRepository = reservationRepository;
            _userRepository = userRepository;
            _seatRepository = seatRepository;
            _mailService = mailService;
            _mapper = mapper;
        }

        // POST: api/Reservations
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 201)]
        public async Task<IActionResult> CreateReservation([FromBody] CreateReservationRequest request)
        {
            if (await _seatRepository.IsReserved(request.Seats, request.ShowId))
                return BadRequest("One or more of the seats are already reserved.");

            var reservation = _mapper.Map<Reservation>(request);
            var user = await _userRepository.Get(reservation.User.Email);
            if (user == null) await _userRepository.Add(reservation.User);
            else
            {
                user.FirstName = reservation.User.FirstName;
                user.LastName = reservation.User.LastName;
                user.PhoneNumber = reservation.User.PhoneNumber;
                await _userRepository.Update(user);
            }
            
            reservation.Code = Guid.NewGuid();
            reservation.User = user;
            reservation.Seats = await _seatRepository.List(request.Seats);
            await _reservationRepository.Add(reservation);

#pragma warning disable 4014
            _mailService.SendMail(reservation.User, reservation.Code.ToString());
#pragma warning restore 4014
            return CreatedAtRoute(new { id = reservation.Id }, new { Result = "Done" });
        }
    }
}
