using ApiServer.Extensions;
using ApiServer.Models.Entities;
using ApiServer.Payloads.Requests;
using ApiServer.Payloads.Responses;
using ApiServer.Persistence.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class HallsController : ControllerBase
    {
        private readonly IHallRepository _hallRepository;
        private readonly ISeatRepository _seatRepository;
        private readonly IMapper _mapper;

        public HallsController(IHallRepository hallRepository, ISeatRepository seatRepository, IMapper mapper)
        {
            _hallRepository = hallRepository;
            _seatRepository = seatRepository;
            _mapper = mapper;
        }

        // GET: api/Halls
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ListHallsResponse), 200)]
        public async Task<IActionResult> ListHalls()
        {
            var halls = await _hallRepository.List();
            var response = _mapper.Map<ListHallsResponse>(halls);
            return Ok(response);
        }

        // GET: api/Halls/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(GetHallResponse), 200)]
        public async Task<IActionResult> GetHall(int id)
        {
            var hall = await _hallRepository.Get(id);
            if (hall == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<GetHallResponse>(hall);
            return Ok(response);
        }

        // PUT: api/Halls/5
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> UpdateHall(int id, [FromBody] UpdateHallRequest request)
        {
            if (id != request.Id || id == 0)
            {
                return BadRequest("The id is invalid.");
            }

            var original = await _hallRepository.Get(request.Id);
            if (original == null)
            {
                return BadRequest("The id is invalid.");
            }

            var hall = _mapper.Map<Hall>(request);
            hall.Seats.SetValue(x => x.HallId = hall.Id);
            
            var removedSeats = original.Seats.Except(hall.Seats);
            if (removedSeats.Any())
            {
                try
                {
                    await _seatRepository.Remove(removedSeats);
                }
                catch (Exception e)
                {
                    return BadRequest();
                }
            }
            
            var addedSeats = hall.Seats.Except(original.Seats);
            if (addedSeats.Any())
            {
                try
                {
                    await _seatRepository.Add(addedSeats);
                }
                catch (Exception e)
                {
                    return BadRequest();
                }
            }

            original.Name = hall.Name;
            await _hallRepository.Update(original);
            return NoContent();
        }

        // POST: api/Halls
        [HttpPost]
        [ProducesResponseType(typeof(void), 201)]
        public async Task<IActionResult> CreateHall([FromBody] CreateHallRequest request)
        {
            var hall = _mapper.Map<Hall>(request);
            await _hallRepository.Add(hall);
            return CreatedAtRoute(new { id = hall.Id }, new { Result = "Done" });
        }

        // DELETE: api/Halls/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> DeleteHall(int id)
        {
            var hall = await _hallRepository.Get(id);
            if (hall == null)
            {
                return NotFound();
            }

            hall.Active = false;
            await _hallRepository.Update(hall);
            return NoContent();
        }
    }
}
