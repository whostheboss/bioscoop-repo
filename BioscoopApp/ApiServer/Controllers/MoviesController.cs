using ApiServer.Extensions;
using ApiServer.Models.Entities;
using ApiServer.Payloads.Requests;
using ApiServer.Payloads.Responses;
using ApiServer.Persistence.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ApiServer.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;

        public MoviesController(IMovieRepository movieRepository, IMapper mapper)
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        // GET: api/Movies
        [HttpGet("Current")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ListMoviesResponse), 200)]
        public async Task<IActionResult> ListCurrentMovies()
        {
            var movies = await _movieRepository.List(true);
            var response = _mapper.Map<ListMoviesResponse>(movies);
            return Ok(response);
        }

        // GET: api/Movies
        [HttpGet]
        [ProducesResponseType(typeof(ListMoviesResponse), 200)]
        public async Task<IActionResult> ListMovies()
        {
            var movies = await _movieRepository.List();
            var response = _mapper.Map<ListMoviesResponse>(movies);
            return Ok(response);
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(void), 2404)]
        [ProducesResponseType(typeof(GetMovieResponse), 200)]
        public async Task<IActionResult> GetMovie(int id)
        {
            var movie = await _movieRepository.Get(id);
            if (movie == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<GetMovieResponse>(movie);
            return Ok(response);
        }

        // PUT: api/Movies/5
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> UpdateMovie(int id, [FromBody] UpdateMovieRequest request)
        {
            if (id != request.Id || id == 0)
            {
                return BadRequest();
            }

            var movie = _mapper.Map<Movie>(request);
            await _movieRepository.Update(movie);
            return NoContent();
        }

        // POST: api/Movies
        [HttpPost]
        [ProducesResponseType(typeof(void), 201)]
        public async Task<IActionResult> CreateMovie([FromBody] CreateMovieRequest request)
        {
            var movie = _mapper.Map<Movie>(request);
            await _movieRepository.Add(movie);
            return CreatedAtRoute(new { id = movie.Id }, new { result = "Done" });
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _movieRepository.Get(id);
            if (movie == null)
            {
                return NotFound();
            }

            movie.Active = false;
            movie.Shows.SetValue(x => x.Active = false);
            await _movieRepository.Update(movie);
            
            return NoContent();
        }
    }
}
