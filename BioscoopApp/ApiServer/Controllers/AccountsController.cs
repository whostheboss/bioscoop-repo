using ApiServer.Models;
using ApiServer.Payloads.Requests;
using ApiServer.Payloads.Responses;
using ApiServer.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ApiServer.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IMapper _mapper;

        public AccountsController(IAuthenticationService authenticationService, IMapper mapper)
        {
            _authenticationService = authenticationService;
            _mapper = mapper;
        }

        //Only to be used for creating accounts with an encrypted password.
        //// POST: api/Accounts/Register
        //[HttpPost("Register")]
        //[ProducesResponseType(typeof(void), 400)]
        //[ProducesResponseType(typeof(AuthenticateResponse), 200)]
        //public async Task<IActionResult> Register([FromBody] AuthenticateRequest model)
        //{
        //    var credentials = _mapper.Map<Credentials>(model);
        //    var token = await _authenticationService.Register(credentials);
        //    if (token == null)
        //        return BadRequest();

        //    var response = new AuthenticateResponse(token);
        //    return Ok(response);
        //}

        // POST: api/Accounts/Login
        [HttpPost("Login")]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(AuthenticateResponse), 200)]
        public async Task<IActionResult> Login([FromBody] AuthenticateRequest model)
        {
            var credentials = _mapper.Map<Credentials>(model);
            var token = await _authenticationService.Authenticate(credentials);
            if (token == null)
                return BadRequest("Username or password is incorrect.");

            var response = new AuthenticateResponse(token);
            return Ok(response);
        }
    }
}
