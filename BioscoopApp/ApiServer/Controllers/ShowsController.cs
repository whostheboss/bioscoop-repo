using ApiServer.Extensions;
using ApiServer.Models.Entities;
using ApiServer.Payloads.Requests;
using ApiServer.Payloads.Responses;
using ApiServer.Persistence.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ShowsController : ControllerBase
    {
        private readonly IShowRepository _showRepository;
        private readonly IMapper _mapper;

        public ShowsController(IShowRepository showRepository, IMapper mapper)
        {
            _showRepository = showRepository;
            _mapper = mapper;
        }

        // GET: api/Shows
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ListShowsResponse), 200)]
        public async Task<IActionResult> ListShows()
        {
            var shows = await _showRepository.List();
            var response = _mapper.Map<ListShowsResponse>(shows);
            return Ok(response);
        }

        // GET: api/Shows/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(GetShowResponse), 200)]
        public async Task<IActionResult> GetShow(int id)
        {
            var show = await _showRepository.Get(id);
            if (show == null)
            {
                return NotFound();
            }
            
            show.Hall.Seats.Where(x => x.Reservations.Any(y => y.ShowId == id)).SetValue(x => x.Occupied = true);
            var response = _mapper.Map<GetShowResponse>(show);
            return Ok(response);
        }

        // PUT: api/Shows/5
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> UpdateShow(int id, [FromBody] UpdateShowRequest request)
        {
            if (id != request.Id || id == 0)
            {
                return BadRequest();
            }

            var show = _mapper.Map<Show>(request);
            await _showRepository.Update(show);
            return NoContent();
        }

        // POST: api/Shows
        [HttpPost]
        [ProducesResponseType(typeof(void), 201)]
        public async Task<IActionResult> CreateShow([FromBody] CreateShowRequest request)
        {
            var show = _mapper.Map<Show>(request);
            await _showRepository.Add(show);
            return CreatedAtRoute(new { id = show.Id }, new { Result = "Done" });
        }

        // DELETE: api/Shows/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(void), 204)]
        public async Task<IActionResult> DeleteShow(int id)
        {
            var show = await _showRepository.Get(id);
            if (show == null)
            {
                return NotFound();
            }

            show.Active = false;
            await _showRepository.Update(show);
            return NoContent();
        }
    }
}
