﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class GetMovieResponse
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Title { get; set; }
        
        [Required]
        public string Description { get; set; }
        
        [Required]
        public double Duration { get; set; }
        
        [Required]
        public DateTime ReleaseDate { get; set; }
        
        [Required]
        public string FeaturedImage { get; set; }
        
        [Required]
        public string Trailer { get; set; }

        [Required]
        public List<ResponseItemClassification> Classifications { get; set; }
        
        [Required]
        public List<ResponseItemShow> Shows { get; set; }

        public class ResponseItemClassification
        {
            [Required]
            public string Name { get; set; }
            
            [Required]
            public string Description { get; set; }
            
            [Required]
            public string ImgSource { get; set; }
        }

        public class ResponseItemShow
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public DateTime StartTime { get; set; }
        }
    }
}