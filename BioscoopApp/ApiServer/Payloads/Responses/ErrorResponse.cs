﻿using ApiServer.Models;

namespace ApiServer.Payloads.Responses
{
    public class ErrorResponse
    {
        public Error Error { get; set; }
    }
}