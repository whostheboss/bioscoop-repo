﻿using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class AuthenticateResponse
    {
        [Required]
        public string Token { get; }

        public AuthenticateResponse(string token)
        {
            Token = token;
        }
    }
}