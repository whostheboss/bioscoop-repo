﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class ListHallsResponse : List<ListHallsResponse.ResponseItem>
    {
        public class ResponseItem
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public string Name { get; set; }
            
            [Required]
            public int SeatCount { get; set; }
        }
    }
}