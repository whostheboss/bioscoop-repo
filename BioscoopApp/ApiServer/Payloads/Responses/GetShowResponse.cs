﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class GetShowResponse
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public DateTime StartTime { get; set; }
        
        [Required]
        public ResponseItemHall Hall { get; set; }
        
        [Required]
        public ResponseItemMovie Movie { get; set; }
        
        public class ResponseItemHall
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public string Name { get; set; }
            
            [Required]
            public List<ResponseItemRow> Rows { get; set; }
        }

        public class ResponseItemMovie
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public string Title { get; set; }
            
            [Required]
            public string Description { get; set; }

            [Required]
            public string FeaturedImage { get; set; }
        }
        
        public class ResponseItemRow
        {
            [Required]
            public int Number { get; set; }
            
            [Required]
            public List<ResponseItemSeat> Seats { get; set; }
        }
        
        public class ResponseItemSeat
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public int Number { get; set; }
            
            [Required]
            public bool Occupied { get; set; }
        }
    }
}