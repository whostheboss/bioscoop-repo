﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class ListMoviesResponse : List<ListMoviesResponse.ResponseItem>
    {
        public class ResponseItem
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public string Title { get; set; }
            
            [Required]
            public string Description { get; set; }
            
            [Required]
            public double Duration { get; set; }
            
            [Required]
            public DateTime ReleaseDate { get; set; }
            
            [Required]
            public string FeaturedImage { get; set; }
            
            [Required]
            public string Trailer { get; set; }

            [Required]
            public List<ResponseItemClassification> Classifications { get; set; }
        }
        
        public class ResponseItemClassification
        {
            [Required]
            public string Name { get; set; }
            
            [Required]
            public string Description { get; set; }
            
            [Required]
            public string ImgSource { get; set; }
        }
    }
}