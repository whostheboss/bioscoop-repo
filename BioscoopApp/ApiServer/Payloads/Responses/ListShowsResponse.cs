﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class ListShowsResponse : List<ListShowsResponse.ResponseItem>
    {
        public class ResponseItem
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public DateTime StartTime { get; set; }
            
            [Required]
            public ResponseItemHall Hall { get; set; }
            
            [Required]
            public ResponseItemMovie Movie { get; set; }
        }

        public class ResponseItemHall
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public string Name { get; set; }
        }

        public class ResponseItemMovie
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public string Title { get; set; }
        }
    }
}