﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Responses
{
    public class GetHallResponse
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public List<ResponseItemRow> Rows { get; set; }

        public class ResponseItemRow
        {
            [Required]
            public int Number { get; set; }
            
            [Required]
            public List<ResponseItemSeat> Seats { get; set; }
        }

        public class ResponseItemSeat
        {
            [Required]
            public int Id { get; set; }
            
            [Required]
            public int Number { get; set; }
        }
    }
}