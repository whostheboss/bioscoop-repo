﻿using ApiServer.Models.Entities;
using ApiServer.Payloads.Requests;
using ApiServer.Payloads.Responses;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace ApiServer.Payloads.Converters
{
    public partial class SeatConverter : IValueConverter<ICollection<Seat>, List<GetHallResponse.ResponseItemRow>>
    {
        public List<GetHallResponse.ResponseItemRow> Convert(ICollection<Seat> sourceMember, ResolutionContext context)
        {
            var items = new List<GetHallResponse.ResponseItemRow>();
            foreach (var seat in sourceMember)
            {
                var item = items.FirstOrDefault(x => x.Number == seat.Row);
                if (item == null)
                {
                    item = new GetHallResponse.ResponseItemRow
                    {
                        Number = seat.Row,
                        Seats = new List<GetHallResponse.ResponseItemSeat>()
                    };
                    items.Add(item);
                }
                item.Seats.Add(new GetHallResponse.ResponseItemSeat
                {
                    Id = seat.Id,
                    Number = seat.Number
                });
            }
            return items;
        }
    }
    
    public partial class SeatConverter : IValueConverter<ICollection<Seat>, List<GetShowResponse.ResponseItemRow>>
    {
        List<GetShowResponse.ResponseItemRow> IValueConverter<ICollection<Seat>, List<GetShowResponse.ResponseItemRow>>.Convert(ICollection<Seat> sourceMember, ResolutionContext context)
        {
            var items = new List<GetShowResponse.ResponseItemRow>();
            foreach (var seat in sourceMember)
            {
                var item = items.FirstOrDefault(x => x.Number == seat.Row);
                if (item == null)
                {
                    item = new GetShowResponse.ResponseItemRow
                    {
                        Number = seat.Row,
                        Seats = new List<GetShowResponse.ResponseItemSeat>()
                    };
                    items.Add(item);
                }
                item.Seats.Add(new GetShowResponse.ResponseItemSeat
                {
                    Id = seat.Id,
                    Number = seat.Number,
                    Occupied = seat.Occupied
                });
            }
            return items;
        }
    }

    public partial class SeatConverter : IValueConverter<ICollection<Seat>, int>
    {
        int IValueConverter<ICollection<Seat>, int>.Convert(ICollection<Seat> sourceMember, ResolutionContext context)
        {
            return sourceMember.Count;
        }
    }

    public partial class SeatConverter : IValueConverter<int[], List<Seat>>
    {
        public List<Seat> Convert(int[] sourceMember, ResolutionContext context)
        {
            return sourceMember.Select(x => new Seat
            {
                Id = x
            }).ToList();
        }
    }

    public partial class SeatConverter : IValueConverter<List<CreateHallRequest.RequestItemRow>, ICollection<Seat>>
    {
        public ICollection<Seat> Convert(List<CreateHallRequest.RequestItemRow> sourceMember, ResolutionContext context)
        {
            var items = new List<Seat>();
            foreach (var row in sourceMember)
            {
                foreach (var seat in row.Seats)
                {
                    items.Add(new Seat
                    {
                        Row = row.Number,
                        Number = seat.Number
                    });
                }
            }
            return items;
        }
    }

    public partial class SeatConverter : IValueConverter<List<UpdateHallRequest.RequestItemRow>, ICollection<Seat>>
    {
        public ICollection<Seat> Convert(List<UpdateHallRequest.RequestItemRow> sourceMember, ResolutionContext context)
        {
            var items = new List<Seat>();
            foreach (var row in sourceMember)
            {
                foreach (var seat in row.Seats)
                {
                    items.Add(new Seat
                    {
                        Id = seat.Id,
                        Row = row.Number,
                        Number = seat.Number
                    });
                }
            }
            return items;
        }
    }
}