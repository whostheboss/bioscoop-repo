﻿using AutoMapper;
using System;

namespace ApiServer.Payloads.Converters
{
    public partial class TimeSpanConverter : IValueConverter<TimeSpan, double>
    {
        public double Convert(TimeSpan sourceMember, ResolutionContext context)
        {
            return sourceMember.TotalMilliseconds;
        }
    }
    
    public partial class TimeSpanConverter : IValueConverter<double, TimeSpan>
    {
        public TimeSpan Convert(double sourceMember, ResolutionContext context)
        {
            return TimeSpan.FromMilliseconds(sourceMember);
        }
    }
}