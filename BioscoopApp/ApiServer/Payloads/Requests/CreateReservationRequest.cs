﻿using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Requests
{
    public class CreateReservationRequest
    {
        [Required(ErrorMessage = "The id of the show is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The id of the show is required.")]
        public int ShowId { get; set; }
        
        [Required(ErrorMessage = "The user is required.")]
        public RequestItemUser User { get; set; }
        
        [Required(ErrorMessage = "The seats are required.")]
        [MinLength(1, ErrorMessage = "At least one seat is required.")]
        public int[] Seats { get; set; }

        public class RequestItemUser
        {
            [Required(ErrorMessage = "The email address is required.")]
            [MaxLength(320, ErrorMessage = "The email address is too long.")]
            [EmailAddress(ErrorMessage = "The email address is not valid.")]
            public string Email { get; set; }

            [Required(ErrorMessage = "The first name is required.")]
            [MaxLength(100, ErrorMessage = "The first name is too long.")]
            public string FirstName { get; set; }

            [Required(ErrorMessage = "The last name is required.")]
            [MaxLength(100, ErrorMessage = "The last name is too long.")]
            public string LastName { get; set; }

            [Required(ErrorMessage = "The phone number is required.")]
            [MaxLength(20, ErrorMessage = "The phone number is not valid.")]
            [Phone(ErrorMessage = "The phone number is not valid.")]
            public string PhoneNumber { get; set; }
        }
    }
}