﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Requests
{
    public class CreateMovieRequest
    {
        [Required(ErrorMessage = "The title is required.")]
        [MaxLength(200, ErrorMessage = "The title is too long.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "The description is required.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "The duration is required.")]
        [Range(0, double.MaxValue, ErrorMessage = "The duration is invalid.")]
        public double Duration { get; set; }

        [Required(ErrorMessage = "The release date is required.")]
        public DateTime ReleaseDate { get; set; }

        [Required(ErrorMessage = "The link to the featured image is required.")]
        [MaxLength(2048, ErrorMessage = "The link to the featured image is too long.")]
        [Url(ErrorMessage = "The link to the featured image is invalid.")]
        public string FeaturedImage { get; set; }

        [MaxLength(2048, ErrorMessage = "The link to the trailer is too long.")]
        [Url(ErrorMessage = "The link to the trailer is invalid.")]
        public string Trailer { get; set; }
    }
}