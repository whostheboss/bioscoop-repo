﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Requests
{
    public class UpdateHallRequest
    {
        [Required(ErrorMessage = "The id is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The id is invalid.")]
        public int Id { get; set; }

        [Required(ErrorMessage = "The name is required.")]
        [MaxLength(100, ErrorMessage = "The name is too long.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The rows are required.")]
        [MinLength(1, ErrorMessage = "At least one row is required.")]
        public List<RequestItemRow> Rows { get; set; }

        public class RequestItemRow
        {
            [Required(ErrorMessage = "The number is required.")]
            [Range(1, int.MaxValue, ErrorMessage = "The number is invalid.")]
            public int Number { get; set; }

            [Required(ErrorMessage = "the seats are required.")]
            [MinLength(1, ErrorMessage = "At least one seat is required.")]
            [MaxLength(20, ErrorMessage = "More than 20 seats is not allowed.")]
            public List<RequestItemSeat> Seats { get; set; }
        }

        public class RequestItemSeat
        {
            [Required(ErrorMessage = "The id is required.")]
            public int Id { get; set; }
            
            [Required(ErrorMessage = "The number is required.")]
            [Range(1, int.MaxValue, ErrorMessage = "The number is invalid.")]
            public int Number { get; set; }
        }
    }
}