﻿using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Requests
{
    public class AuthenticateRequest
    {
        [Required(ErrorMessage = "The username is required.")]
        public string UserName { get; set; }
        
        [Required(ErrorMessage = "The password is required.")]
        public string Password { get; set; }
    }
}