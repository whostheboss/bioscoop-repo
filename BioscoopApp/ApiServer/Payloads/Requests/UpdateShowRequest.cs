﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Payloads.Requests
{
    public class UpdateShowRequest
    {
        [Required(ErrorMessage = "The id is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The id is invalid.")]
        public int Id { get; set; }

        [Required(ErrorMessage = "The hall id is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The hall id is invalid.")]
        public int HallId { get; set; }

        [Required(ErrorMessage = "The movie id is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The movie id is invalid.")]
        public int MovieId { get; set; }

        [Required(ErrorMessage = "The start time is required.")]
        public DateTime StartTime { get; set; }
    }
}