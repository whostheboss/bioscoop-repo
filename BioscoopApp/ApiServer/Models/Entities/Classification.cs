using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Models.Entities
{
    public class Classification
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        
        public string Description { get; set; }

        [Required]
        [MaxLength(2048)]
        public string ImgSource { get; set; }
        
        public virtual ICollection<Movie> Movies { get; set; }
    }
}