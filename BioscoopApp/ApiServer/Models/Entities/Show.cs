using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiServer.Models.Entities
{
    public class Show
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public int HallId { get; set; }
        
        [Required]
        public int MovieId { get; set; }
        
        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; } = true;

        [ForeignKey(nameof(HallId))]
        public virtual Hall Hall { get; set; }
        
        [ForeignKey(nameof(MovieId))]
        public virtual Movie Movie { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}