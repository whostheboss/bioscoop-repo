﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiServer.Models.Entities
{
    public class Seat
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public int HallId { get; set; }
        
        [Required]
        public int Row { get; set; }
        
        [Required]
        public int Number { get; set; }
        
        [NotMapped]
        public bool Occupied { get; set; }

        [ForeignKey(nameof(HallId))]
        public virtual Hall Hall { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; }

        protected bool Equals(Seat other)
        {
            return HallId == other.HallId && Row == other.Row && Number == other.Number;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Seat) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(HallId, Row, Number);
        }
    }
}