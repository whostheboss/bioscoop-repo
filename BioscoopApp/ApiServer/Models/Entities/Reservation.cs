﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiServer.Models.Entities
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public int ShowId { get; set; }
        
        [Required]
        public int UserId { get; set; }
        
        [Required]
        public Guid Code { get; set; }
        
        [ForeignKey(nameof(ShowId))]
        public virtual Show Show { get; set; }
        
        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        public virtual ICollection<Seat> Seats { get; set; }
    }
}