using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Models.Entities
{
    public class Hall
    {
        [Key]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "The name is required.")]
        [MaxLength(100, ErrorMessage = "The name is too long.")]
        public string Name { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; } = true;

        public virtual ICollection<Show> Shows { get; set; }

        public virtual ICollection<Seat> Seats { get; set; }
    }
}