﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApiServer.Models.Entities
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public TimeSpan Duration { get; set; }

        [Required]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [MaxLength(2048)]
        public string FeaturedImage { get; set; }

        [MaxLength(2048)]
        public string Trailer { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; } = true;

        public virtual ICollection<Classification> Classifications { get; set; }

        public virtual ICollection<Show> Shows { get; set; }
    }
}