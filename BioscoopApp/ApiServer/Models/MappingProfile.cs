﻿using ApiServer.Models.Entities;
using ApiServer.Payloads.Converters;
using ApiServer.Payloads.Requests;
using ApiServer.Payloads.Responses;
using AutoMapper;

namespace ApiServer.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AuthenticateRequest, Credentials>();
            
            
            CreateMap<Movie, ListMoviesResponse.ResponseItem>().ForMember(dest => dest.Duration, opt => opt.ConvertUsing(new TimeSpanConverter(), src => src.Duration));
            CreateMap<Classification, ListMoviesResponse.ResponseItemClassification>();
            
            CreateMap<Movie, GetMovieResponse>().ForMember(dest => dest.Duration, opt => opt.ConvertUsing(new TimeSpanConverter(), src => src.Duration));
            CreateMap<Classification, GetMovieResponse.ResponseItemClassification>();
            CreateMap<Show, GetMovieResponse.ResponseItemShow>();

            CreateMap<CreateMovieRequest, Movie>().ForMember(dest => dest.Duration, opt => opt.ConvertUsing(new TimeSpanConverter(), src => src.Duration));
            CreateMap<UpdateMovieRequest, Movie>().ForMember(dest => dest.Duration, opt => opt.ConvertUsing(new TimeSpanConverter(), src => src.Duration));


            CreateMap<Hall, ListHallsResponse.ResponseItem>().ForMember(dest => dest.SeatCount, opt => opt.ConvertUsing(new SeatConverter(), src => src.Seats));
            CreateMap<Hall, GetHallResponse>().ForMember(dest => dest.Rows, opt => opt.ConvertUsing(new SeatConverter(), src => src.Seats));
            
            CreateMap<CreateHallRequest, Hall>().ForMember(dest => dest.Seats, opt => opt.ConvertUsing(new SeatConverter(), src => src.Rows));
            CreateMap<UpdateHallRequest, Hall>().ForMember(dest => dest.Seats, opt => opt.ConvertUsing(new SeatConverter(), src => src.Rows));


            CreateMap<CreateReservationRequest, Reservation>().ForMember(dest => dest.Seats, opt => opt.ConvertUsing(new SeatConverter(), src => src.Seats));
            CreateMap<CreateReservationRequest.RequestItemUser, User>();


            CreateMap<Show, ListShowsResponse.ResponseItem>();
            CreateMap<Hall, ListShowsResponse.ResponseItemHall>();
            CreateMap<Movie, ListShowsResponse.ResponseItemMovie>();
            
            CreateMap<Show, GetShowResponse>();
            CreateMap<Hall, GetShowResponse.ResponseItemHall>().ForMember(dest => dest.Rows, opt => opt.ConvertUsing(new SeatConverter(), src => src.Seats));
            CreateMap<Movie, GetShowResponse.ResponseItemMovie>();

            CreateMap<CreateShowRequest, Show>();
            
            CreateMap<UpdateShowRequest, Show>();
        }
    }
}