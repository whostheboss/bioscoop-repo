﻿namespace ApiServer.Models.Settings
{
    public class AuthenticationSettings
    {
        public string Secret { get; set; }
    }
}