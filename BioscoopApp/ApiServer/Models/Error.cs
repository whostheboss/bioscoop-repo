﻿using System.Collections.Generic;

namespace ApiServer.Models
{
    public class Error
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Target { get; set; }
        public List<ErrorDetail> Details { get; set; }
        public InnerError InnerError { get; set; }
    }

    public class ErrorDetail
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Target { get; set; }
    }

    public class InnerError
    {
        public string Code { get; set; }
        public InnerError NestedInnerError { get; set; }
    }
}