﻿using ApiServer.Models.Settings;
using ApiServer.Models.Entities;
using Microsoft.Extensions.Options;
using MimeKit;
using QRCoder;
using System.IO;
using System.Threading.Tasks;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace ApiServer.Services
{
    public interface IMailService
    {
        Task SendMail(User user, string data);
    }

    public class MailService : IMailService
    {
        private readonly SmtpSettings _settings;
        private readonly QRCodeGenerator _qrGenerator;

        public MailService(IOptions<SmtpSettings> options)
        {
            _settings = options.Value;
            _qrGenerator = new QRCodeGenerator();
        }

        public async Task SendMail(User user, string data)
        {
            var qrCodeData = _qrGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new PngByteQRCode(qrCodeData);
            await using var memoryStream = new MemoryStream(qrCode.GetGraphic(20));
            var mail = new MimeMessage();
            mail.From.Add(new MailboxAddress("Cinema", _settings.Email));
            mail.To.Add(new MailboxAddress($"{user.FirstName} {user.LastName}", user.Email));
            mail.Subject = "Reservation code";
            var bodyBuilder = new BodyBuilder
            {
                TextBody = "Attached, you'll find the qr-code for your reservation. Show this at the counter."
            };
            bodyBuilder.Attachments.Add("ReservationCode.bmp", memoryStream);
            mail.Body = bodyBuilder.ToMessageBody();
            await SendMail(mail);
        }

        private async Task SendMail(MimeMessage mail)
        {
            using var client = new SmtpClient();
            await client.ConnectAsync(_settings.Host, _settings.Port);
            await client.AuthenticateAsync(_settings.UserName, _settings.Password);
            await client.SendAsync(mail);
        }
    }
}