﻿using ApiServer.Extensions;
using ApiServer.Models;
using ApiServer.Models.Settings;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ApiServer.Services
{
    public interface IAuthenticationService
    {
        Task<string> Register(Credentials credentials);
        Task<string> Authenticate(Credentials credentials);
    }
    
    public class AuthenticationService : IAuthenticationService
    {
        private readonly AuthenticationSettings _settings;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        private readonly JwtSecurityTokenHandler _tokenHandler;
        private readonly SymmetricSecurityKey _securityKey;

        public AuthenticationService(IOptions<AuthenticationSettings> options, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _settings = options.Value;
            _userManager = userManager;
            _signInManager = signInManager;

            _tokenHandler = new JwtSecurityTokenHandler();
            _securityKey = new SymmetricSecurityKey(_settings.Secret.ToByteArray());
        }

        public async Task<string> Register(Credentials credentials)
        {
            var result = await _userManager.CreateAsync(new IdentityUser(credentials.UserName), credentials.Password);
            if (!result.Succeeded) return null;

            return GenerateToken(credentials.UserName);
        }
        
        public async Task<string> Authenticate(Credentials credentials)
        {
            var result = await _signInManager.PasswordSignInAsync(credentials.UserName, credentials.Password, true, false);
            if (!result.Succeeded) return null;

            return GenerateToken(credentials.UserName);
        }

        private string GenerateToken(string user)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("user", user) }),
                //Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(_securityKey, SecurityAlgorithms.HmacSha512Signature)
            };
            var token = _tokenHandler.CreateToken(tokenDescriptor);
            return _tokenHandler.WriteToken(token);
        }
    }
}