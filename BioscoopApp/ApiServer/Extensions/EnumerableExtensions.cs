﻿using System;
using System.Collections.Generic;

namespace ApiServer.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> SetValue<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var item in items)
            {
                action(item);
            }

            return items;
        }
    }
}