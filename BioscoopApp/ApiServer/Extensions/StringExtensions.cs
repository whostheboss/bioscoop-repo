﻿using System.Text;

namespace ApiServer.Extensions
{
    public static class StringExtensions
    {
        public static byte[] ToByteArray(this string value) => ToByteArray(value, Encoding.ASCII);
        public static byte[] ToByteArray(this string value, Encoding encoding)
        {
            return encoding.GetBytes(value);
        }
    }
}